function createCell(header, type) {
    if (header === void 0) { header = false; }
    if (type === void 0) { type = 'input'; }
    var c1 = document.createElement(header && 'th' || 'td');
    if (type === 'input') {
        c1.appendChild(document.createElement('input'));
    }
    else {
        c1.appendChild(document.createTextNode(type));
    }
    return c1;
}
function reloadMath() {
    MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
}
function constructBySize() {
    var size = Number(document.getElementById("size").value);
    console.log(size);
    var table = document.createElement('table');
    var headerRow = document.createElement('tr');
    var x = document.createElement('th');
    x.appendChild(document.createTextNode("$x$"));
    var y = document.createElement('th');
    y.appendChild(document.createTextNode("$y$"));
    headerRow.appendChild(x);
    headerRow.appendChild(y);
    table.appendChild(headerRow);
    for (var i = 0; i < size; i++) {
        var row = document.createElement('tr');
        var c1 = createCell();
        var c2 = createCell();
        row.appendChild(c1);
        row.appendChild(c2);
        table.appendChild(row);
    }
    var tableDiv = document.getElementById("table_result");
    while (tableDiv.firstChild) {
        tableDiv.removeChild(tableDiv.firstChild);
    }
    tableDiv = document.getElementById("table");
    while (tableDiv.firstChild) {
        tableDiv.removeChild(tableDiv.firstChild);
    }
    tableDiv.appendChild(table);
    tableDiv.appendChild(document.createElement('br'));
    var submitButton = document.createElement('button');
    submitButton.innerHTML = "Submit";
    submitButton.onclick = collectInput;
    tableDiv.appendChild(submitButton);
    reloadMath();
}
function collectInput() {
    var table = document.getElementById("table").childNodes[0];
    var rows = table.childNodes;
    var numRows = rows.length;
    var x = new Array(), y = new Array();
    for (var i = 1; i < numRows; i++) {
        var row = rows[i];
        var cols = row.childNodes;
        var col1 = Number(cols[0].childNodes[0].value);
        var col2 = Number(cols[1].childNodes[0].value);
        x.push(col1);
        y.push(col2);
    }
    var mu_x = mean(x), mu_y = mean(y), xy = x.map(function (v, i, _) { return v * y[i]; }), mu_xy = mean(xy), x_mux = x.map(function (v, _, __) { return v - mu_x; }), y_muy = y.map(function (v, _, __) { return v - mu_y; }), x_mux_sq = x_mux.map(function (v, _, __) { return v * v; }), y_muy_sq = y_muy.map(function (v, _, __) { return v * v; }), dx = mean(x_mux_sq), dy = mean(y_muy_sq), covxy = mu_xy - (mu_x * mu_y), r = covxy / (Math.sqrt(dx * dy)), stdx = Math.sqrt(dx), stdy = Math.sqrt(dy), a = r * stdy / stdx, b = mu_y - (mu_x * stdy * r / stdx);
    console.log(mu_x, mu_y, xy, x_mux, y_muy, x_mux_sq, y_muy_sq);
    var header = ['$x$', '$y$', '$xy$', '$x - \\mu_x$', '$y - \\mu_y$', '$(x - \\mu_x)^2$', '$(y - \\mu_y)^2$'];
    var data = [x, y, xy, x_mux, y_muy, x_mux_sq, y_muy_sq];
    var resTable = document.createElement('table');
    var headerRow = document.createElement('tr');
    for (var i = 0; i < header.length; i++) {
        headerRow.appendChild(createCell(true, header[i]));
    }
    resTable.appendChild(headerRow);
    for (var i = 0; i < x.length; i++) {
        var row = document.createElement('tr');
        for (var j = 0; j < header.length; j++) {
            row.appendChild(createCell(false, fixPoint(data[j][i])));
        }
        resTable.appendChild(row);
    }
    var tableDiv = document.getElementById("table_result");
    while (tableDiv.firstChild) {
        tableDiv.removeChild(tableDiv.firstChild);
    }
    tableDiv.appendChild(document.createElement('br'));
    tableDiv.appendChild(document.createTextNode("$$r(X,Y) = \\dfrac{cov(X,Y)}{\\sqrt{D[X] \\times D[y]}} = \\dfrac{\\mu_{xy} - \\mu_x\\mu_y}{\\sqrt{D[X] \\times D[Y]}}$$"));
    tableDiv.appendChild(document.createTextNode("$$\\mu_x = " + fixPoint(mu_x) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(document.createTextNode("$$\\mu_y = " + fixPoint(mu_y) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(document.createTextNode("$$\\mu_{xy} = " + fixPoint(mu_xy) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(document.createTextNode("$$cov(X,Y) = " + fixPoint(covxy) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(document.createTextNode("$$D[X] = " + fixPoint(dx) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(document.createTextNode("$$D[Y] = " + fixPoint(dy) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(document.createTextNode("$$r(X,Y) = " + fixPoint(r) + "$$"));
    // tableDiv.appendChild(document.createElement('br'))
    tableDiv.appendChild(resTable);
    var bcalc = document.createElement("div");
    bcalc.appendChild(document.createElement('br'));
    bcalc.appendChild(document.createTextNode("Assuming the linearity:\n $$y = ax+b$$ and $$a = \\dfrac{r_{xy}\\sigma_y}{\\sigma_x}$$ and $$b = \\mu_y - \\dfrac{\\mu_x \\sigma_y r_{xy}}{\\sigma_x}$$"));
    bcalc.appendChild(document.createElement('br'));
    bcalc.appendChild(document.createTextNode("Finding $\\sigma_x$ and $\\sigma_y$:"));
    bcalc.appendChild(document.createElement('br'));
    bcalc.appendChild(document.createTextNode("$$\\sigma_y=\\sqrt{D[X]}=" + fixPoint(stdx) + "$$"));
    bcalc.appendChild(document.createElement('br'));
    bcalc.appendChild(document.createTextNode("$$\\sigma_y=\\sqrt{D[Y]}=" + fixPoint(stdy) + "$$"));
    bcalc.appendChild(document.createElement('br'));
    bcalc.appendChild(document.createTextNode("Finding $a$ and $b$:"));
    bcalc.appendChild(document.createTextNode("$$a = \\frac{" + fixPoint(r) + "\\times " + fixPoint(stdy) + "}{" + fixPoint(stdx) + "}" + "=" + fixPoint(a) + "$$"));
    bcalc.appendChild(document.createElement('br'));
    bcalc.appendChild(document.createTextNode("$$b = " + fixPoint(mu_y) + " - \\frac{" + fixPoint(mu_x) + "\\times" + fixPoint(stdy) + "\\times" + fixPoint(r) + "}{" + fixPoint(stdx) + "}" + "=" + fixPoint(b) +
        "$$"));
    tableDiv.appendChild(bcalc);
    reloadMath();
}
var mean = function (array) {
    return array.reduce(function (a, b, _, __) { return a + b; }) / array.length;
};
function fixPoint(number) {
    if (number.toString() == Math.round(number).toString()) {
        return number.toString();
    }
    else {
        return number.toFixed(3);
    }
}
